package com.practica.practica.cloudapis.controllers;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import com.practica.practica.cloudapis.services.NasaService;
import com.practica.practica.core.libs.javax.IValidationSqlInput;
import com.practica.practica.core.libs.serviceresponse.ErrorServiceResponseException;
import com.practica.practica.core.libs.serviceresponse.ServiceResponseFactory;

@CrossOrigin(origins = "*")
@RestController
@Validated
@RequestMapping("/api/nasa/")
public class NasaController {


	@Autowired
	NasaService nasa_service;

	@GetMapping("image-day")
	public	ResponseEntity<String> getCurrentImage() throws ErrorServiceResponseException{

		try{
			ServiceResponseFactory response_service = nasa_service.getPictureToday();
			return response_service.getResponse();
		}catch(ErrorServiceResponseException exeption){
			return exeption.getServiceResponse().getResponseError();
		}
	}
	
	@GetMapping("images-range/{start_date}/{end_date}")
	public	ResponseEntity<String> imageByRange(@Valid @PathVariable("start_date") @IValidationSqlInput String start_date,
												@Valid @PathVariable("end_date") @IValidationSqlInput String end_date ){
		
		try{
			ServiceResponseFactory response_service  = nasa_service.getLisPictures(start_date,end_date);
			return response_service.getResponse();
		}catch(ErrorServiceResponseException exeption){
			return exeption.getServiceResponse().getResponseError();
		}
	}

}