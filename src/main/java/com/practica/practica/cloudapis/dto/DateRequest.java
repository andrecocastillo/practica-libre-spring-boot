package com.practica.practica.cloudapis.dto;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DateRequest {
    @NotNull(message="La fecha es requerida")
    @Size(min=1, max=30, message="Ingrese un nombre valido")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(value=TemporalType.TIMESTAMP)
    private String date_input;
}
