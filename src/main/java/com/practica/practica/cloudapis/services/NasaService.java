package com.practica.practica.cloudapis.services;

import java.util.List;

import com.practica.practica.cloudapis.dto.ImageDTO;
import com.practica.practica.core.constants.ApiKeys;
import com.practica.practica.core.constants.NasaEndpoints;
import com.practica.practica.core.libs.serviceresponse.ErrorServiceResponseException;
import com.practica.practica.core.libs.serviceresponse.ServiceResponseConst;
import com.practica.practica.core.libs.serviceresponse.ServiceResponseFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersSpec;

@Service
public class NasaService {
 
  
    NasaService(){ }

    public ServiceResponseFactory getPictureToday(){
        RequestHeadersSpec<?> requestHeaders = WebClient.create().
                                        get().uri(NasaEndpoints.URL_IMAGE_TODAY);

        ImageDTO image_body = requestHeaders.retrieve().toEntity(ImageDTO.class).block().getBody();
        
        return new ServiceResponseFactory(ServiceResponseConst.SUCCESS,image_body,"Imagen obtenida con exito.");
    }

    public ServiceResponseFactory getLisPictures(String start_date,String end_date){

        String query_params = "&start_date="+ start_date +"&end_date="+ end_date;
        RequestHeadersSpec<?> requestHeaders = WebClient.create().
                                        get().uri(NasaEndpoints.URL_IMAGE_TODAY + query_params);

        List<ImageDTO> list_images = requestHeaders.retrieve().toEntityList(ImageDTO.class).block().getBody();
        
        return new ServiceResponseFactory(ServiceResponseConst.SUCCESS,list_images,"Listado de imagenes obtenido con exito");
    }
}