package com.practica.practica.core.libs;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;

public class Helpers{

    private final static Logger LOGGER = Logger.getLogger(Helpers.class.getName());

    public static void msg(String titulo, Object txt){
        System.out.println("----------" + titulo + "-----------");
        System.out.println("--");
        System.out.println(txt);
        System.out.println("--");
        System.out.println("---------------------");
    }

    public static void log(Object txt){
        System.out.println("---------------------");
        System.out.println("--");
        System.out.println(txt);
        System.out.println("--");
        System.out.println("---------------------");
    }   

    public static void loginfo(String info){
        System.out.println("---------------------");
        System.out.println("---------------------");
        LOGGER.info(info);            
        System.out.println("---------------------");
        System.out.println("---------------------");
    }

    public static void logwarn(String info){
        System.out.println("---------------------");
        LOGGER.warning(info);        
        System.out.println("---------------------");
    }
    /*
    public static int getCuentaID(){
        return SistemaConst.CUENTA_ID;
    }

    public static int getUserID(){
        return SistemaConst.USUARIO_ID;
    }*/
    
    public static void die(String text){
        System.out.println("---------------------");
        System.out.println("---------------------");
        System.out.println("---------------------");
        System.out.println("---------------------");
        System.out.println("---------------------");

        LOGGER.info(text);
        throw new ArithmeticException(text); 
    }

    public static void varDump(Object info){
        String infoS  = new Gson().toJson(info);
        System.out.println(infoS);
    } 

    
    public static void varDumpList(List<? extends Object> info){
        for(Object objInfo : info){
            String infoS  = new Gson().toJson(objInfo);
            System.out.println(infoS);
            System.out.println(" ----- ");
        }
    }

}