package com.practica.practica.core.libs.serviceresponse;


public class ErrorServiceResponse extends ServiceResponse{

    public ErrorServiceResponse(Object data_response,String message){
        this.response = false;
        this.status_code = 200;
        this.message = message;

        this.setDataResponse(data_response);
    }

}
