package com.practica.practica.core.libs.serviceresponse;

/*
public class ErrorServiceResponseException extends Exception{

    public ErrorServiceResponseException() {
    }
}
*/


public class ErrorServiceResponseException extends Exception{

    private transient ServiceResponseFactory serviceresponse;

    public ErrorServiceResponseException(ServiceResponseFactory serviceresponse) {
        this.serviceresponse = serviceresponse;
    }

    public ServiceResponseFactory getServiceResponse(){
        return this.serviceresponse;
    }
}
