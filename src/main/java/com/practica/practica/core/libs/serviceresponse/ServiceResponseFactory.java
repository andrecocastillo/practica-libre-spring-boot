package com.practica.practica.core.libs.serviceresponse;

import org.springframework.http.ResponseEntity;

public class ServiceResponseFactory{

    ResponseEntity<String> response;
    private String response_final;
    
    public ServiceResponseFactory(String response_type,Object data_response,String message){
        
        if(response_type.equals(ServiceResponseConst.SUCCESS)){
            this.response =  new SussesServiceResponse(data_response,message).responseJSON();
        }else if(response_type.equals(ServiceResponseConst.ERROR)){
            this.response =  new ErrorServiceResponse(data_response,message).responseJSON();
        }
        
        this.response_final = response_type;
    }


    public ResponseEntity<String> getResponse() throws ErrorServiceResponseException{
        
        if(this.response_final.equals(ServiceResponseConst.ERROR)){
            throw new ErrorServiceResponseException(this);
        }

        return this.response;
    }

    public ResponseEntity<String> getResponseError(){
        return this.response;
    }

}