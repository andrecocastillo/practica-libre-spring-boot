package com.practica.practica.core.libs;

import org.yaml.snakeyaml.constructor.Construct;

import com.google.gson.Gson;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

public class ServiceResponse {
	
	private boolean response = false;
	private String data_response = "";
	private String message = "";
	private int status_code = 0;
	
	public ServiceResponse(){
		
	}
	
	public void buildResponse(Boolean response,String data_response, String message) {
		this.response = response;
		this.data_response = data_response;
		this.message = message;
	}
	
	public void buildResponse(Boolean response,Object data_response, String message) {
		this.response = response;
		this.data_response = new Gson().toJson(data_response);
		this.message = message;
	}

	public void buildBadRequestResponse(Object data_response, String message) {
		this.status_code = 400;
		this.response = false;
		this.data_response = new Gson().toJson(data_response);
		this.message = message;
	}
	
	public ResponseEntity<String> responseJSON(){
		HttpHeaders responseHeaders = new HttpHeaders();
	    responseHeaders.set("Content-Type","application/json");
		
		String r = this.toString();
		if(this.status_code == 400){
			return ResponseEntity.badRequest().headers(responseHeaders).body(r.toString());
		}else{
			return ResponseEntity.ok().headers(responseHeaders).body(r.toString());
		}
    }
	
	public String toString() {
		if(this.data_response == null){ this.data_response = "[]"; }
        return "{\"res\":\""+this.response+"\",\"dataObj\":"+this.data_response+",\"msg\":\""+this.message+"\"}";
	}	
}