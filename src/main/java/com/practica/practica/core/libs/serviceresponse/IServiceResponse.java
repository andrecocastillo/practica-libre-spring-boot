package com.practica.practica.core.libs.serviceresponse;

import org.springframework.http.ResponseEntity;

interface IServiceResponse {
    ResponseEntity<String> getResponse();
}
