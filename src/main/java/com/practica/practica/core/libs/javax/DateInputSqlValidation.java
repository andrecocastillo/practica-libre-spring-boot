package com.practica.practica.core.libs.javax;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DateInputSqlValidation implements ConstraintValidator<IValidationSqlInput, String> {

    private static final java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        try{
            java.util.Date ret = sdf.parse(value.trim());
            if (sdf.format(ret).equals(value.trim())) {
                return true;
            }else{
                return false;
            }
        }catch(Exception e){
            return false;
        }
    }
}