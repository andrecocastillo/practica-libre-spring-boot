package com.practica.practica.core.libs.serviceresponse;


public class SussesServiceResponse extends ServiceResponse{

    public SussesServiceResponse(Object data_response,String message){
        this.response = true;
        this.status_code = 200;
        this.message = message;
        this.setDataResponse(data_response);
    }

}
