package com.practica.practica.core.libs;

import javax.validation.ConstraintViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalControllerExceptionHandler {
  
    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<String> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        ServiceResponse response = new ServiceResponse();
        Object object_empty = new Object();
        response.buildBadRequestResponse(object_empty,"Los parametros enviados no son validos");
        return response.responseJSON();
    }

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<String> constraintExeption(ConstraintViolationException ex) {
        ServiceResponse response = new ServiceResponse();
        Object object_empty = new Object();
        response.buildBadRequestResponse(object_empty,"El parametro personalizado no es valido");
        return response.responseJSON();
    }

}