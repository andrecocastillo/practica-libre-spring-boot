package com.practica.practica.core.constants;

public class NasaEndpoints {
    public static final String URL_IMAGE_TODAY = "https://api.nasa.gov/planetary/apod?api_key=" + ApiKeys.getKeyNasaAccount();
}
