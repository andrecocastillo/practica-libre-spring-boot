package com.practica.practica.core.constants;

public class ApiKeys {
    

    public static String getKeyNasaAccount(){
        // As a practicar exercise i will not save APIKEY_NASA in the application
        // but in the system variables
        return System.getenv("APIKEY_NASA");
    }

}