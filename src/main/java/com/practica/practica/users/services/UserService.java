package com.practica.practica.users.services;


import java.util.List;
import java.util.Optional;

import com.practica.practica.core.libs.DozerHelper;
import com.practica.practica.core.libs.Helpers;
import com.practica.practica.core.libs.ServiceResponse;
import com.practica.practica.core.libs.serviceresponse.ErrorServiceResponseException;
import com.practica.practica.core.libs.serviceresponse.ServiceResponseConst;
import com.practica.practica.core.libs.serviceresponse.ServiceResponseFactory;
import com.practica.practica.core.libs.serviceresponse.SussesServiceResponse;
import com.practica.practica.users.controllers.requests.UserInformationRequest;
import com.practica.practica.users.entities.User;
import com.practica.practica.users.repositories.UserRepository;
import com.practica.practica.users.services.responses.UserResponse;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserService{

	private UserRepository user_repository;

	@Autowired
	Mapper mapper;
	
	UserService(UserRepository user_repository) {
		this.user_repository = user_repository;
	}


	public ServiceResponseFactory save(UserInformationRequest user_data) throws ErrorServiceResponseException{
		try{
			User user_information = mapper.map(user_data, User.class);
			this.user_repository.save(user_information);
			
			return new ServiceResponseFactory(ServiceResponseConst.SUCCESS,null,"Usuario guardado con exito.");
		}catch(Exception e){
			throw new ErrorServiceResponseException(new ServiceResponseFactory(ServiceResponseConst.ERROR,null,"El usuario no se pudo guardar."));
		}
	}

	public ServiceResponseFactory updateDataUser(UserInformationRequest user_information) throws ErrorServiceResponseException {
		try{
			User user_data = mapper.map(user_information, User.class);
			Optional<User> exists_user =  this.user_repository.findById(user_data.getId());
			if (exists_user.isPresent()) {
				User current_data_user = exists_user.get();
				current_data_user.setName( user_data.getName() );
				current_data_user.setEmail( user_data.getEmail() );
				this.user_repository.save(current_data_user);

				return new ServiceResponseFactory(ServiceResponseConst.SUCCESS,null,"Usuario actualizado con exito.");
			}else{
				throw new ErrorServiceResponseException(new ServiceResponseFactory(ServiceResponseConst.ERROR,null,"El usuario solicitado no existe"));
			}
		}catch(Exception e){
			throw new ErrorServiceResponseException(new ServiceResponseFactory(ServiceResponseConst.ERROR,null,"EL usuario no se pudo actualizar"));
		}
	}


	public ServiceResponseFactory getSingleInformation(int user_id) throws ErrorServiceResponseException {
		try{
			Optional<User> exist_user =  this.user_repository.findById(user_id);
			if (exist_user.isPresent()) {
				User user_information = exist_user.get();
				UserResponse contactResponse = mapper.map(user_information, UserResponse.class);

				return new ServiceResponseFactory(ServiceResponseConst.SUCCESS,contactResponse,"Datos de usuario.");
			} else {
				return new ServiceResponseFactory(ServiceResponseConst.ERROR,null,"Datos de usuario");
			}
		}catch(Exception e){
			throw new ErrorServiceResponseException(new ServiceResponseFactory(ServiceResponseConst.ERROR,null,"Informacion errorea de usuario"));
		}
	}

	public ServiceResponseFactory getAllUsersInformation()  throws ErrorServiceResponseException{
		try{
			List<User> list_users =  this.user_repository.findAll();
			// Helpers.varDumpList(list_users);
			// UserResponse contactResponse = mapper.map(list_users, UserResponse.class);
			List<UserResponse> list_users_mapping = DozerHelper.map(mapper, list_users, UserResponse.class);
			return new ServiceResponseFactory(ServiceResponseConst.SUCCESS,list_users_mapping,"Listado de usuarios generado con exito.");
		}catch(Exception e){
			return new ServiceResponseFactory(ServiceResponseConst.ERROR,null,"Error al obtener el listado de usuarios");
		}
	}
}