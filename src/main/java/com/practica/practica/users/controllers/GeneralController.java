package com.practica.practica.users.controllers;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import com.practica.practica.core.libs.serviceresponse.ErrorServiceResponseException;
import com.practica.practica.core.libs.serviceresponse.ServiceResponseFactory;
import com.practica.practica.users.controllers.requests.UserInformationRequest;
import com.practica.practica.users.entities.User;
import com.practica.practica.users.services.UserService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/users/")
public class GeneralController {


	@Autowired
	private UserService user_service;
	
	@Autowired
	Mapper mapper;

	@CrossOrigin(origins = "*")
	@PostMapping("")
	public	ResponseEntity<String> save(@Valid @RequestBody UserInformationRequest user_information){
		try{
			ServiceResponseFactory response_service  = user_service.save(user_information);
			return response_service.getResponse();
		}catch(ErrorServiceResponseException e){
			return e.getServiceResponse().getResponse();
		}
	}

	@CrossOrigin(origins = "*")
	@PutMapping("/{user_id}")
	public	ResponseEntity<String> updateDataUser(
			@PathVariable("user_id") int user_id,
			@Valid @RequestBody UserInformationRequest user_information
	){
		try{
			user_information.setId(user_id);
			ServiceResponseFactory service_response = user_service.updateDataUser(user_information);
			return service_response.getResponse();
		}catch(ErrorServiceResponseException e){
			return e.getServiceResponse().getResponse();
		}
	}

	@CrossOrigin(origins = "*")
	@GetMapping("/{user_id}")
	public	ResponseEntity<String> getSingleInformation(@PathVariable("user_id") int user_id){
		try{
			ServiceResponseFactory service_response = user_service.getSingleInformation(user_id);
			return service_response.getResponse();
		}catch(ErrorServiceResponseException e){
			return e.getServiceResponse().getResponse();
		}
	}

	@CrossOrigin(origins = "*")
	@GetMapping("/")
	public	ResponseEntity<String> getAllUsersInformation()	{
		try{
			ServiceResponseFactory service_response = user_service.getAllUsersInformation();
			return service_response.getResponse();
		}catch(ErrorServiceResponseException e){
			return e.getServiceResponse().getResponse();
		}
	}
}