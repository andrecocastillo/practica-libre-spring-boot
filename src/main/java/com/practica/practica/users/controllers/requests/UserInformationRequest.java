package com.practica.practica.users.controllers.requests;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class UserInformationRequest{
 

    private int id;

    @NotNull(message="El nombre es requerido")
    @Size(min=1, max=30, message="Ingrese un nombre valido")
    private String name;
    
    @NotNull(message="El nombre es requerido")
    @Size(min=1, max=255, message="Inrgese un email valido")
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}